/**The MIT License (MIT)

Copyright (c) 2018 by Daniel Eichhorn - ThingPulse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

See more at https://thingpulse.com
*/
#include <vector>

#include <Arduino.h>

#include <ESP8266WiFiMulti.h>
#include <WiFiClientSecure.h>
#include <ESPHTTPClient.h>
#include <JsonListener.h>

// time
#include <time.h>                       // time() ctime()
#include <sys/time.h>                   // struct timeval
#include "SSD1306Wire.h"
#include "OLEDDisplayUi.h"
#include <PubSubClient.h>

#include "Wire.h"
#include "OpenWeatherMapCurrent.h"
#include "OpenWeatherMapForecast.h"
#include "WeatherStationFonts.h"
#include "WeatherStationImages.h"
#include "WeatherStationConfig.h"

#include <Adafruit_BMP085.h>
#include "BH1750FVI.h"
#include <DHT.h>

/***************************
 * Begin Settings
 **************************/

#define BUTTON D3   // flash button

/***************************
 * Begin DHT11 Settings
 **************************/
WiFiClient client;
#define pin 14       // ESP8266-12E  D5 read emperature and Humidity data

/***************************Wi
 * Begin Atmosphere and Light Sensor Settings
 **************************/
const int Light_ADDR = 0b0100011;   // address:0x23
const int Atom_ADDR = 0b1110111;  // address:0x77
double readLight();
double readAtmosphere();
Adafruit_BMP085 bmp;
BH1750FVI light_sensor(Light_ADDR);

/***************************
 * DHT settings
 */
#define DHTTYPE DHT11
#define DHTPIN 14

DHT dht(DHTPIN, DHTTYPE);

double readTemperature();
double readHumidity();

/*
 * Rain sensor settings
 * FC-37 + YL-38
 */

#define RAIN_PIN 12

unsigned long mqtt_last_retry = 0;

PubSubClient mqtt_client(client);
void mqtt_publish(String, String);
void mqtt_publish(String, double);
void mqtt_publish_stats();
void mqtt_reconnect();

/***************************
 * Begin Settings
 **************************/

// Setup
const int UPDATE_INTERVAL_SECS = 20 * 60; // Update every 20 minutes

// Display Settings
const int I2C_DISPLAY_ADDRESS = 0x3c;
#if defined(ESP8266)
const int SDA_PIN = D2;
const int SDC_PIN = D1;
#else
const int SDA_PIN = 5; //D3;
const int SDC_PIN = 4; //D4;
#endif

double temperature = 0.0;
double humidity = 0.0;
double light = 0.0;
int atmosphere = 0;

/***************************
* End Settings
**************************/
// Initialize the oled display for address 0x3c
// sda-pin=14 and sdc-pin=12
SSD1306Wire     display(I2C_DISPLAY_ADDRESS, SDA_PIN, SDC_PIN);
OLEDDisplayUi   ui( &display );

OpenWeatherMapCurrentData currentWeather;
OpenWeatherMapCurrent currentWeatherClient;

OpenWeatherMapForecastData forecasts[MAX_FORECASTS];
OpenWeatherMapForecast forecastClient;

time_t now;

// flag changed in the ticker function every 10 minutes
bool readyForWeatherUpdate = false;

String lastUpdate = "--";

long timeSinceLastWUpdate = 0;

//declaring prototypes
void drawProgress(OLEDDisplay *display, int percentage, String label);
void updateData(OLEDDisplay *display);
void drawStats(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawSensorData(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawCurrentWeather(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawForecast(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawBlankScreen(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawForecastDetails(OLEDDisplay *display, int x, int y, int dayIndex);
void drawHeaderOverlay(OLEDDisplay *display, OLEDDisplayUiState* state);
void setReadyForWeatherUpdate();


// Add frames
// this array keeps function pointers to all frames
// frames are the single views that slide from right to left
FrameCallback frames[] = { drawStats, drawSensorData, drawCurrentWeather, drawForecast, drawBlankScreen };
uint8_t numberOfFrames = 5;
uint8_t blankFrameId = 4;

OverlayCallback overlays[] = { drawHeaderOverlay };
int numberOfOverlays = 1;

ESP8266WiFiMulti wifiMulti;
const uint32_t connectTimeoutMs = 5000;

ICACHE_RAM_ATTR void next() {
    ui.nextFrame();
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  Wire.begin(SDA_PIN, SDC_PIN);
  
  Wire.beginTransmission(Atom_ADDR);
  //initialize Atmosphere sensor
  if (!bmp.begin()) {
    Serial.println("Could not find BMP180 or BMP085 sensor at 0x77");
  }else{
    Serial.println("Found BMP180 or BMP085 sensor at 0x77");
  }
  Wire.endTransmission();

  //initialize light sensor
  light_sensor.powerOn();
  light_sensor.setContHighRes();

  // initialize dispaly
  display.init();
  display.clear();
  display.display();

  //display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setContrast(255);

  dht.begin();

  // WiFi setup
  WiFi.mode(WIFI_STA);
  std::vector<WifiAP> wifiAPs(std::begin(wifiAP_array), std::end(wifiAP_array));
  for (std::vector<WifiAP>::iterator it = wifiAPs.begin(); it != wifiAPs.end(); ++it)
      wifiMulti.addAP(it->ssid.c_str(), it->passwd.c_str());

  // Get time from network time service
  configTime(TZ_SEC, DST_SEC, "10.1.2.1", "0.arch.pool.ntp.org", "0.pool.ntp.org");

  mqtt_client.setServer(mqtt_server, 1883);

  ui.setTargetFPS(30);

  ui.setActiveSymbol(activeSymbole);
  ui.setInactiveSymbol(inactiveSymbole);

  // You can change this to
  // TOP, LEFT, BOTTOM, RIGHT
  ui.setIndicatorPosition(BOTTOM);

  // Defines where the first frame is located in the bar.
  ui.setIndicatorDirection(LEFT_RIGHT);

  // You can change the transition that is used
  // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_TOP, SLIDE_DOWN
  ui.setFrameAnimation(SLIDE_LEFT);
  ui.disableAutoTransition();

  ui.setFrames(frames, numberOfFrames);

  ui.setOverlays(overlays, numberOfOverlays);

  // Inital UI takes care of initalising the display too.
  ui.init();

  Serial.println("");

  setReadyForWeatherUpdate();

  pinMode(BUTTON, INPUT);
  attachInterrupt(digitalPinToInterrupt(BUTTON), next, FALLING);
}

void loop() {
  static long read_time = 0;
  static long publish_time = 0;

  // Maintain WiFi connection

  int counter = 0;
  while (WiFi.status() != WL_CONNECTED) {
    wifiMulti.run(connectTimeoutMs);
    delay(500);
    Serial.print(".");
    display.clear();
    display.drawString(64, 10, "Connecting to WiFi");
    display.drawXbm(46, 30, 8, 8, counter % 3 == 0 ? activeSymbole : inactiveSymbole);
    display.drawXbm(60, 30, 8, 8, counter % 3 == 1 ? activeSymbole : inactiveSymbole);
    display.drawXbm(74, 30, 8, 8, counter % 3 == 2 ? activeSymbole : inactiveSymbole);
    display.display();

    counter++;
  }

  if (!mqtt_client.connected()) {
    mqtt_reconnect();
  }

  if (millis() - timeSinceLastWUpdate > (1000L*UPDATE_INTERVAL_SECS)) {
    setReadyForWeatherUpdate();
  }

  if (readyForWeatherUpdate && ui.getUiState()->frameState == FIXED) {
    updateData(&display);
  }

  mqtt_client.loop();

  if (read_time > millis()) {
    read_time = 0;
  }
  if (publish_time > millis()) {
    publish_time = 0;
  }

  //Read sensors every 3 seconds
  if(millis() - read_time > 3000L){
    atmosphere = readAtmosphere();
    humidity = readHumidity();
    light = readLight();
    temperature = readTemperature();
    
    read_time = millis();

    Serial.print("Sensor reading at ");
    Serial.println(read_time);

    Serial.print("\tAtmospheric pressure: ");
    Serial.print(atmosphere);
    Serial.println("Pa");
    
    Serial.print("\tHumidity: ");
    Serial.print(humidity);
    Serial.println("%");

    Serial.print("\tLight: ");
    Serial.print(light);
    Serial.println("");
    
    Serial.print("\tTemperature: ");
    Serial.print(temperature);
    Serial.println("°C");

    // Compensation factor for light sensor
    light_sensor.setTemperature(floor(temperature));
  }

  // publish data every 60 seconds
  if(millis() - publish_time > 60000L) {
    char* data;

    // get the necessary size of data
    size_t n = snprintf(data, 0, data_fmt, temperature, humidity, atmosphere, light) + 1 /* the '\0' byte */;
    
    // alloc and format data
    data = (char*)malloc(n * sizeof(char));
    if (data == NULL) return; // not enough memory: don't publish and try again later
    snprintf(data, n, data_fmt, temperature, humidity, atmosphere / 100.0, light);

    publish_time = millis();
    mqtt_publish(topic_feed, data);
    free(data);
    /*
    mqtt_publish(topic_temperature, temperature);
    mqtt_publish(topic_humidity, humidity);
    mqtt_publish(topic_atmosphere, atmosphere / 100.0);
    mqtt_publish(topic_light, light);
    */
  }

  int remainingTimeBudget = ui.update();

  if (remainingTimeBudget > 0) {
    // You can do some work here
    // Don't do stuff if you are below your
    // time budget.
    delay(remainingTimeBudget);
  }
}

void drawProgress(OLEDDisplay *display, int percentage, String label) {
  display->clear();
  if (ui.getUiState()->currentFrame == blankFrameId)
      return;
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  display->drawString(64, 10, label);
  display->drawProgressBar(2, 28, 124, 10, percentage);
  display->display();
}

void updateData(OLEDDisplay *display) {
  drawProgress(display, 10, "Updating time...");
  drawProgress(display, 30, "Updating weather...");

  String city = "";

  WiFiClientSecure ipApiClient;
  ipApiClient.setInsecure();
  Serial.print("[HTTPS] begin...\n");
  HTTPClient https;
  //client.setInsecure();
  if (https.begin(ipApiClient, "https://ipapi.co/city")) {
      int httpCode = https.GET();
      if (httpCode > 0) {
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
              city = https.getString();
              Serial.println(city);
          }
      } else {
          Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }
      https.end();
  } else {
      Serial.printf("[HTTPS] Unable to connect\n");
  }

  currentWeatherClient.setMetric(IS_METRIC);
  currentWeatherClient.setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  if (city == "") 
      currentWeatherClient.updateCurrentById(&currentWeather, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION_ID);
  else
      currentWeatherClient.updateCurrent(&currentWeather, OPEN_WEATHER_MAP_APP_ID, city);
  drawProgress(display, 50, "Updating forecasts...");

  forecastClient.setMetric(IS_METRIC);
  forecastClient.setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  uint8_t allowedHours[] = {12};
  forecastClient.setAllowedHours(allowedHours, sizeof(allowedHours));
  if (city == "") 
      forecastClient.updateForecastsById(forecasts, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION_ID, MAX_FORECASTS);
  else
      forecastClient.updateForecasts(forecasts, OPEN_WEATHER_MAP_APP_ID, city, MAX_FORECASTS);
  readyForWeatherUpdate = false;
  timeSinceLastWUpdate = millis();
  drawProgress(display, 100, "Done...");
  delay(1000);
}

void drawStats(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  now = time(nullptr);
  struct tm* timeInfo;
  timeInfo = localtime(&now);
  char buff[16];
  const char smiley_ok[3] = {':', ')', '\0'};
  const char smiley_nok[3] = {':', '(', '\0'};

  display->setTextAlignment(TEXT_ALIGN_LEFT);
  
  display->setFont(ArialMT_Plain_10);
  String date = WDAY_NAMES[timeInfo->tm_wday];
  sprintf_P(buff, PSTR("%s, %02d/%02d/%04d"), WDAY_NAMES[timeInfo->tm_wday].c_str(), timeInfo->tm_mday, timeInfo->tm_mon+1, timeInfo->tm_year + 1900);
  display->drawString(0 + x, 0 + y, String(buff));

  display->setFont(ArialMT_Plain_16);
  sprintf_P(buff, PSTR("%02d:%02d:%02d"), timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec);
  display->drawString(0 + x, 12 + y, String(buff));

  display->setFont(ArialMT_Plain_10);
  display->drawString(0 + x, 28 + y, WiFi.SSID());
  display->drawString(0 + x, 38 + y, client.localIP().toString());

  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_10);

  // MQTT
  sprintf_P(buff, PSTR("MQTT %s"), mqtt_client.connected() ? smiley_ok : smiley_nok );
  display->drawString(125 + x, 24 + y, String(buff));

  // Forecasts
  sprintf_P(buff, PSTR("OWM %s"), (millis() - timeSinceLastWUpdate < (1000L * UPDATE_INTERVAL_SECS)) ? smiley_ok : smiley_nok);
  display->drawString(125 + x, 36 + y, String(buff));

  // Time - NTP?

  display->setTextAlignment(TEXT_ALIGN_LEFT);
}

void drawSensorData(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  double pressure_hpa = atmosphere / 100.0;
  char buff[14];

  display->setFont(ArialMT_Plain_10);
  
  display->setTextAlignment(TEXT_ALIGN_RIGHT);  
  display->drawString(128 + x, 0 + y, "on-board sensors");

  display->setTextAlignment(TEXT_ALIGN_LEFT);
  sprintf_P(buff, PSTR("%0.0flx"), light);
  display->drawString(0 + x, 12 + y, String(buff));

  display->setTextAlignment(TEXT_ALIGN_RIGHT);  
  sprintf_P(buff, PSTR("%0.2fhPa"), pressure_hpa);
  display->drawString(128 + x, 12 + y, String(buff));

  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_16);
  sprintf_P(buff, PSTR("%0.0f%%"), humidity);
  display->drawString(128 + x, 26 + y, String(buff));

  display->setTextAlignment(TEXT_ALIGN_LEFT);
  sprintf_P(buff, PSTR("%0.1f°C"), temperature);
  display->drawString(0 + x, 26 + y, String(buff));  
}

void drawCurrentWeather(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128 + x, 0 + y, "openweathermap");

  display->setFont(Meteocons_Plain_21);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0 + x, 4 + y, currentWeather.iconMeteoCon);

  display->setFont(ArialMT_Plain_16);
  String line = String(currentWeather.temp, 1) + (IS_METRIC ? "°C" : "°F");
  display->drawString(26 + x, 10 + y, line);

  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  line = String(currentWeather.humidity) + "%";
  display->drawString(128 + x, 10 + y, line);

  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  line = String(currentWeather.tempMin, 1) + "°C/" + String(currentWeather.tempMax, 1) + "°C";
  display->drawString(0 + x, 26 + y, line);

  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  line = String(currentWeather.pressure) + "hPa";
  display->drawString(128 + x, 26 + y, line);

  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128 + x, 36 + y, currentWeather.description);

  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0 + x, 36 + y, currentWeather.cityName);

  /*
  owm_data["latitude"] = currentWeather.lat;
  owm_data["longitude"] = currentWeather.lon;
  owm_data["visibility"] = currentWeather.visibility;
  owm_data["wind_speed"] = currentWeather.windSpeed;
  owm_data["wind_degrees"] = currentWeather.windDeg;
  owm_data["sunrise"] = currentWeather.sunrise;
  owm_data["sunset"] = currentWeather.sunset;
  owm_data["location"] = currentWeather.cityName;
   */
}


void drawForecast(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  drawForecastDetails(display, x, y, 0);
  drawForecastDetails(display, x + 44, y, 1);
  drawForecastDetails(display, x + 88, y, 2);
}

void drawForecastDetails(OLEDDisplay *display, int x, int y, int dayIndex) {
  time_t observationTimestamp = forecasts[dayIndex].observationTime;
  struct tm* timeInfo;
  timeInfo = localtime(&observationTimestamp);
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  display->drawString(x + 20, y, WDAY_NAMES[timeInfo->tm_wday]);

  display->setFont(Meteocons_Plain_21);
  display->drawString(x + 20, y + 12, forecasts[dayIndex].iconMeteoCon);
  String temp = String(forecasts[dayIndex].temp, 0) + (IS_METRIC ? "°C" : "°F");
  display->setFont(ArialMT_Plain_10);
  display->drawString(x + 20, y + 34, temp);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
}


void drawBlankScreen(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
    display->clear();
}


void drawHeaderOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  now = time(nullptr);
  struct tm* timeInfo;
  timeInfo = localtime(&now);
  char buff[14];

  if (state->currentFrame == blankFrameId) {
      ui.disableAllIndicators();
      return;
  } else {
      ui.enableAllIndicators();
  }

  display->setColor(WHITE);
  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  sprintf_P(buff, PSTR("%02d:%02d"), timeInfo->tm_hour, timeInfo->tm_min);
  display->drawString(0, 54, String(buff));

  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  sprintf_P(buff, PSTR("%0.1f°C"), temperature);
  display->drawString(128, 54, buff);
}

void setReadyForWeatherUpdate() {
  Serial.println("Setting readyForUpdate to true");
  readyForWeatherUpdate = true;
}

void mqtt_publish(String subtopic, double value) {
  char str_value[10];
  sprintf(str_value, "%0.2f", value);
  mqtt_publish(subtopic, String(value));
}

void mqtt_publish(String topic, String value) {
  // MQTT message size: topic name, payload, headers
  unsigned int buffer_size_needed = topic.length() + value.length() + 10;
  if (mqtt_client.getBufferSize() < buffer_size_needed) {
    if (!mqtt_client.setBufferSize(buffer_size_needed)) {
      Serial.print("Failed setting MQTT buffer size from ");
      Serial.print(mqtt_client.getBufferSize());
      Serial.print(" to ");
      Serial.println(buffer_size_needed);
      Serial.print("Won't be sending message to topic ");
      Serial.print(topic);
      Serial.print(" with payload ");
      Serial.println(value);
    }
  }
  if (mqtt_client.connected() && (mqtt_client.getBufferSize() >= buffer_size_needed)) {
    if (mqtt_client.publish(topic.c_str(), value.c_str())) {
      Serial.print("\tPublished to MQTT topic ");
      Serial.println(topic);
      Serial.print("\twith payload ");
      Serial.println(value);
    } else {
      Serial.print("\tFailure publishing to MQTT topic");
      Serial.println(topic);
    }
  } else {
    Serial.print("\tNot publishing ");
    Serial.print(topic);
    Serial.print(" to server: not connected, rc=");
    Serial.println(mqtt_client.state());
  }
}

void mqtt_reconnect() {
  // considering mqtt_last_retry == 0 as the initialiation condition.
  bool force_reconnect = (mqtt_last_retry == 0) ? true : false;
  // check if millis() has overflowed
  if (mqtt_last_retry > millis()) {
    mqtt_last_retry = 0;
  }
  // retries every 2 minutes
  if (((millis() - mqtt_last_retry) > 120000) || force_reconnect) {
    mqtt_last_retry = millis();
    Serial.print("MQTT connecting ");
    if (mqtt_client.connect(mqtt_clientid, mqtt_username, mqtt_password)) {
      Serial.println(" connected");
    } else {
      Serial.print("failed, rc=");
      Serial.println(mqtt_client.state());
    }
  }
}

double readTemperature () {
  return dht.readTemperature();
}

double readHumidity () {
  return dht.readHumidity();
}

double readLight(){
  return light_sensor.getLux();
}

double readAtmosphere(){
  return bmp.readPressure();
}
