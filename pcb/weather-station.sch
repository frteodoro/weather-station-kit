EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor:DHT11 U1
U 1 1 613255E1
P 4600 3000
F 0 "U1" H 4356 3046 50  0000 R CNN
F 1 "DHT11" H 4356 2955 50  0000 R CNN
F 2 "Sensor:Aosong_DHT11_5.5x12.0_P2.54mm" H 4600 2600 50  0001 C CNN
F 3 "http://akizukidenshi.com/download/ds/aosong/DHT11.pdf" H 4750 3250 50  0001 C CNN
	1    4600 3000
	-1   0    0    -1  
$EndComp
$Comp
L ESP8266:NodeMCU_1.0_(ESP-12E) U2
U 1 1 61358C0A
P 3000 3000
F 0 "U2" H 3000 4193 60  0000 C CNN
F 1 "NodeMCU_1.0_(ESP-12E)" H 3000 4087 60  0000 C CNN
F 2 "ESP8266:NodeMCU-LoLinV3" H 3000 3981 60  0000 C CNN
F 3 "" H 2400 2150 60  0000 C CNN
	1    3000 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3000 4300 3000
Wire Wire Line
	4600 3300 4600 3600
Wire Wire Line
	4600 3600 3800 3600
Wire Wire Line
	4600 2700 4900 2700
Wire Wire Line
	4900 2700 4900 3700
Wire Wire Line
	4900 3700 3800 3700
$EndSCHEMATC
