#pragma once

#include <string>

#define TZ     -3   // (utc+) TZ in hours
#define DST_MN  0   // use 60mn for summer time in some countries
#define TZ_MN   ((TZ)*60)
#define TZ_SEC  ((TZ)*3600)
#define DST_SEC ((DST_MN)*60)

// WIFI
struct WifiAP {
    std::string ssid;
    std::string passwd;
};
WifiAP wifiAP_array[] = {
    {"YOUR-NETWORK-SSID", "YOUR-NETWORK-PWD"},
    {"ANOTHER-NETWORK-SSID", "ANOTHER-NETWORK-PWD"},
    {"ADD MORE", "AS NEEDED"}
};

// MQTT settings
const char* mqtt_server   = "mqtt3.thingspeak.com";
const char* mqtt_clientid = "MQTT Client ID";
const char* mqtt_username = "MQTT Username";
const char* mqtt_password = "password generated when MQTT Credentials was created";
const char* topic_feed    = "channels/<channelID>/publish";
const char* data_fmt      = "field1=%0.2f&field2=%0.2f&field3=%0.2f&field4=%0.2f";

// OpenWeatherMap Settings
// Sign up here to get an API key:
// https://docs.thingpulse.com/how-tos/openweathermap-key/
String OPEN_WEATHER_MAP_APP_ID = "";
/*
Go to https://openweathermap.org/find?q= and search for a location. Go through the
result set and select the entry closest to the actual location you want to display 
data for. It'll be a URL like https://openweathermap.org/city/2657896. The number
at the end is what you assign to the constant below.
 */
String OPEN_WEATHER_MAP_LOCATION_ID = "";

// Pick a language code from this list:
// Arabic - ar, Bulgarian - bg, Catalan - ca, Czech - cz, German - de, Greek - el,
// English - en, Persian (Farsi) - fa, Finnish - fi, French - fr, Galician - gl,
// Croatian - hr, Hungarian - hu, Italian - it, Japanese - ja, Korean - kr,
// Latvian - la, Lithuanian - lt, Macedonian - mk, Dutch - nl, Polish - pl,
// Portuguese - pt, Romanian - ro, Russian - ru, Swedish - se, Slovak - sk,
// Slovenian - sl, Spanish - es, Turkish - tr, Ukrainian - ua, Vietnamese - vi,
// Chinese Simplified - zh_cn, Chinese Traditional - zh_tw.
String OPEN_WEATHER_MAP_LANGUAGE = "en";
const uint8_t MAX_FORECASTS = 4;

const boolean IS_METRIC = true;

// Adjust according to your language
const String WDAY_NAMES[] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
const String MONTH_NAMES[] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
